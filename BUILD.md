### Gerar imagem da aplicação

```sh
API_URL=http://localhost:8080
docker build -f docker/Dockerfile -t hello-app . --build-arg API_URL=${API_URL}
```

