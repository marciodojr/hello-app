# Hello app

> My ace Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).


## Local gitlab runner

```
gitlab-runner exec docker docker-build --docker-privileged --env="CI_REGISTRY_PASSWORD=password"
```

## Gitlab Runner

Adicione as variáveis CI_REGISTRY_PASSWORD e API_URL nas variáveis de ambiente do projeto